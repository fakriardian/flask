from flask import Flask, jsonify, make_response
from flask_restful import Api
from jsonschema import ValidationError
import swagger

from resources.KuponPromo import ModelPromoKupon
from resources.KuponForecast import ModelKuponForecast
from resources.NewKuponForecast import ModelNewKuponForecast
from resources.Mitra import ModelMitra
from resources.ProductCategorizer import ModelProductCategorizer


app = Flask("time-series")
app.config["JSON_SORT_KEYS"] = False
api = Api(app)
swagger.config(app)

api.add_resource(ModelPromoKupon, "/predict/promokupon")
api.add_resource(ModelKuponForecast, "/predict/kuponforecast")
api.add_resource(ModelNewKuponForecast,
                 "/predict/kuponforecast/<prod_name>")
api.add_resource(ModelMitra, "/predict/mitra")
api.add_resource(ModelProductCategorizer, "/predict/productcategorizer")


@app.route("/health")
def health():
    """
    docker container health check purpose
    ---
    tags:
      - Heatlh Check
    responses:
      200:
        description: Health Check
        schema:
          type: "object"
          properties:
            health:
              type: "string"
              example: "healthy"
    """
    resp = jsonify(health="healthy")
    resp.status_code = 200
    return resp


@app.errorhandler(400)
def bad_request(error):
    """For bad Request"""
    if isinstance(error.description, ValidationError):
        original_error = error.description
        return make_response(
            jsonify({"ok": False, "message": original_error.message}), 400
        )
    # handle other "Bad Request"-errors
    return error


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=9696)
