import pickle
import pandas as pd
from flask_restful import Resource
from flask import request, jsonify
from flask_expects_json import expects_json
from flasgger import swag_from
from .schema.schema import schema

with open("models/promo_suggestion_logreg_model.sav", "rb") as f_in:
    model_promo = pickle.load(f_in)


class Predict:
    def __init__(self, promo_kupon, model):
        self.promo_kupon = promo_kupon
        self.model = model

        self.feature_cols = ["area", "product_name",
                             "goods_condition", "total_stock"]

    def predict_promo_kupon(self):
        df = pd.read_parquet("datasets/promo_kupon.parquet.gzip")

        X = df[self.feature_cols]  # Features

        X = pd.concat([X, pd.DataFrame(self.promo_kupon)])
        X = pd.get_dummies(X)
        X = X.iloc[-len(self.promo_kupon["area"]):, :]
        y_pred = self.model.predict(X)
        return y_pred


class ModelPromoKupon(Resource):
    @expects_json(schema())
    @swag_from("./swagger/index.yaml")
    def post(self):
        try:
            req_kupon_promo = request.get_json()
            prediction = Predict(
                req_kupon_promo, model_promo).predict_promo_kupon()

            result = {
                "ok": True,
                "prediction_value": prediction.tolist(),
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy",
                },
            }
            return jsonify(result)
        except ValueError:
            error = {
                "ok": False,
                "message": "Column-names while Model building is different!",
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy & irfan",
                },
            }

            return error, 500
