def schema():
    return {
        "type": "object",
        "properties": {
            "area": {"type": "array", "items": {"type": "string"}},
            "product_name": {"type": "array", "items": {"type": "string"}},
            "goods_condition": {"type": "array", "items": {"type": "string"}},
            "total_stock": {"type": "array", "items": {"type": "number"}},
        },
        "required": ["area", "product_name", "goods_condition", "total_stock"],
    }
