import re
import string
import logging
import pickle
from flask_restful import Resource
from flask import request, jsonify
from flask_expects_json import expects_json
from flasgger import swag_from
from .schema.schema import schema

with open("models/TFIDF_LR.sav", "rb") as f:
    model = pickle.load(f)
with open("datasets/label.obj", "rb") as f:
    label = pickle.load(f)


class ProductCategorizer:
    def __init__(self, products, model, labels):
        self.products = products
        self.label_list = labels
        self.model = model
        self.product_name_cleanse = ""

        self.tokens = [
            "pet",
            "carton",
            "sach",
            "sachet",
            "botol",
            "kaleng",
            "iktnx",
            "cup",
            "carton",
            "av",
            "ballx",
            "toples",
            "bungkus",
            "bb",
            "isi",
            "btl",
            "pcs",
            "box",
            "pc",
            "gram",
            "pack",
            "can",
            "gr",
            "sch",
            "sak",
            "bks",
            "jar",
            "xl",
            "bag",
            "scht",
            "toplesx",
            "pcsx",
            "ml",
            "fet",
            "pouch",
            "renceng",
            "can",
            "rcg",
            "cans",
            "rtgx",
            "bksx",
            "karton",
            "bal",
            "kg",
            "kilogram",
            "packx",
            "ltr",
            "liter",
            "all variant",
            "dus",
            "ppn",
            "krt",
            "lusin",
            "lsn",
            "lt",
            "rtg",
            "xrtg",
            "pak",
            "tin",
            "mlx",
            "pcsxkarton",
            "pieces",
            "sht",
            "sst",
            "pls",
            "x pieces",
            "slop",
            " x",
            "tim",
        ]

    def clean(self):
        product_name_cleanse = []
        for product in self.products["name"]:
            product_name = str(product)
            product_name = product_name.lower()
            product_name = re.sub(r"\s+(?i:x)\s+", "", product_name)
            product_name = re.sub(r"[0-9]+\s*[a-zA-Z]+", "", product_name)
            product_name = re.sub(r"\b\d+\b *|\b[a-z]\b *", "", product_name)
            for punctuation in string.punctuation:
                product_name = product_name.replace(punctuation, " ")
            product_name = re.sub(r"\s\s+", " ", product_name)

            wordlist = set(self.tokens)
            product_name = " ".join(
                [word for word in product_name.split() if word.lower() not in wordlist]
            )
            product_name_cleanse.append(product_name)
        self.product_name_cleanse = product_name_cleanse
        logging.info(
            "Product Name after cleansing: {}".format(self.product_name_cleanse)
        )
        return self

    def categorize(self):
        confidence_scr = [
            {cls: round(scr * 100, 2) for cls, scr in zip(self.label_list, y_val)}
            for y_val in self.model.predict_proba(self.product_name_cleanse)
        ]
        result = [
            {
                "product_name": product,
                "product_name_cleansed": cleanse,
                "confidence_scr": scr,
            }
            for product, cleanse, scr in zip(
                self.products["name"], self.product_name_cleanse, confidence_scr
            )
        ]
        return result


class ModelProductCategorizer(Resource):
    @expects_json(schema())
    @swag_from("./swagger/index.yaml")
    def post(self):
        try:
            req_product = request.get_json()
            product_predict = ProductCategorizer(req_product, model, label)
            result = {
                "ok": True,
                "product_predict": product_predict.clean().categorize(),
                "development_information": {
                    "model_by": "andi",
                    "deployed_by": "ariandy",
                },
            }
            return jsonify(result)
        except ValueError:
            error = {
                "ok": False,
                "message": "Column-names while Model building is different!",
                "development_information": {
                    "model_by": "andi",
                    "deployed_by": "ariandy",
                },
            }

            return error, 500
