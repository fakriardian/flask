def schema():
    return {
        "type": "object",
        "properties": {
            "name": {"type": "array", "items": {"type": "string"}},
        },
        "required": ["name"],
    }
