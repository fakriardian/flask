import pickle
import pandas as pd
import numpy as np
from flask_restful import Resource
from flask import request, jsonify
from flask_expects_json import expects_json
from flasgger import swag_from
from .schema.schema import schema

with open("models/minuman_mitra_model.sav", "rb") as f_in:
    model_mitra = pickle.load(f_in)


class Predict:
    def __init__(self, mitra_qty, model):
        self.mitra_qty = mitra_qty
        self.model = model

    def single_mitra(self):
        X = pd.DataFrame(self.mitra_qty)
        y_pred = self.model.predict(X)
        rounded = [np.round(x, 2) for x in y_pred]
        round_int = [int(x) for x in rounded]
        return round_int


class ModelMitra(Resource):
    @expects_json(schema())
    @swag_from("./swagger/index.yaml")
    def post(self):
        try:
            req_mitra_qty = request.get_json()
            prediction = Predict(req_mitra_qty, model_mitra).single_mitra()

            result = {
                "ok": True,
                "forecast_value": prediction,
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy",
                },
            }
            return jsonify(result)
        except ValueError:
            error = {
                "ok": False,
                "message": "Column-names while Model building is different!",
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy & irfan",
                },
            }

            return error, 500
