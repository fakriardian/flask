import pickle
import pandas as pd
import numpy as np
from flask_restful import Resource
from flask import request
from flask_expects_json import expects_json
from flasgger import swag_from
from .schema.schema import schema

with open("models/xgb_kupon_jacknjill.sav", "rb") as f_in:
    model_forecast_kupon = pickle.load(f_in)


class Predict:
    def __init__(self, kupon_forecast_qty, model):
        self.kupon_forecast_qty = kupon_forecast_qty
        self.model = model

    def kupon_forecast(self):
        X = pd.DataFrame(self.kupon_forecast_qty)
        y_pred = self.model.predict(X)
        rounded = [np.round(x, 2) for x in y_pred]
        round_int = [int(x) for x in rounded]
        return round_int


class ModelKuponForecast(Resource):
    @expects_json(schema())
    @swag_from("./swagger/index.yaml")
    def post(kupon_forecast_qty):
        try:
            req_kupon_qty = request.get_json()
            prediction = Predict(req_kupon_qty, model_forecast_kupon).kupon_forecast()

            result = {
                "ok": True,
                "forecast_value": prediction,
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy",
                },
            }
            return result
        except ValueError:
            error = {
                "ok": False,
                "message": "Column-names while Model building is different!",
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy & irfan",
                },
            }

            return error, 500
