import pickle
import pandas as pd
import numpy as np
from flask_restful import Resource
from flask import request
from flask_expects_json import expects_json
from flasgger import swag_from
from .schema.schema import schema
import json

# read
with open('datasets/prod_list.json', 'r') as f:
    prod_list = json.load(f)

#import models
for codename in prod_list:
    exec("reg_{}_loaded = pickle.load(open('models/new_kupon_model/xgb_new_kupon_{}.bin', 'rb'))".format(codename, codename)
         )


class Predict:
    def __init__(self, kupon_forecast_qty, model_id):
        self.kupon_forecast_qty = kupon_forecast_qty
        exec("self.model  = reg_{}_loaded".format(model_id))

    def new_kupon_forecast(self):
        X = pd.DataFrame(self.kupon_forecast_qty)
        y_pred = self.model.predict(X)
        rounded = [np.round(x, 2) for x in y_pred]
        round_int = [int(x) for x in rounded]
        return round_int


class ModelNewKuponForecast(Resource):
    @expects_json(schema())
    @swag_from("./swagger/index.yaml")
    def post(kupon_forecast_qty, prod_name):
        try:
            print('kntl')
            req_kupon_qty = request.get_json()
            model_id = prod_name[:6]
            prediction = Predict(req_kupon_qty, model_id).new_kupon_forecast()

            result = {
                "ok": True,
                "forecast_value": prediction,
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy",
                },
            }
            return result
        except ValueError:
            error = {
                "ok": False,
                "message": "Column-names while Model building is different!",
                "development_information": {
                    "model_by": "abel",
                    "deployed_by": "ariandy & irfan",
                },
            }

            return error, 500
