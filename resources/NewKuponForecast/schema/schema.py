def schema():
    return {
        "type": "object",
        "properties": {
            "dayofweek": {"type": "array", "items": {"type": "number"}},
            "quarter": {"type": "array", "items": {"type": "number"}},
            "month": {"type": "array", "items": {"type": "number"}},
            "year": {"type": "array", "items": {"type": "number"}},
            "dayofyear": {"type": "array", "items": {"type": "number"}},
            "dayofmonth": {"type": "array", "items": {"type": "number"}},
            "weekofyear": {"type": "array", "items": {"type": "number"}},
        },
        "required": [
            "dayofweek",
            "quarter",
            "month",
            "year",
            "dayofyear",
            "dayofmonth",
            "weekofyear",
        ],
    }
