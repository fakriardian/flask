from flasgger import Swagger


def config(app):
    template = {
        "swagger": "2.0",
        "info": {
            "title": "DWH-MODEL-API",
            "description": "API for model dwh team",
            "contact": {"email": "irfan.fakri@pintap.id"},
            "version": "0.0.1",
        },
        "schemes": ["http", "https"],
    }

    app.config["SWAGGER"] = {
        "title": "DWH-MODEL-API",
        "uiversion": 3,
        "specs_route": "/api/",
    }

    Swagger(app, template=template)
