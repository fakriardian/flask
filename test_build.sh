#!/bin/bash

docker container stop dwhcontainer
docker container rm dwhcontainer

docker build -f Dockerfile . -t dwhapp:latest
docker run -d -p 9696:9696 --name dwhcontainer dwhapp:latest 